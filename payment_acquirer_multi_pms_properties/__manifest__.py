# Copyright 2009-2020 Noviat.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Payment Acquirer Multiproperty",
    "author": "Commit [Sun], Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/pms",
    "category": "Generic Modules/Property Management System",
    "version": "2.0.1.0.2",
    "license": "AGPL-3",
    "depends": [
        "pms",
    ],
    "data": [
        "views/payment_acquirer.xml",
    ],
    "installable": True,
}
