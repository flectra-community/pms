# © 2013  Therp BV
# © 2014  ACSONE SA/NV
# Copyright 2018 Quartile Limited
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "multi_pms_properties",
    "summary": "Multi Properties Manager",
    "version": "2.0.1.0.0",
    "website": "https://gitlab.com/flectra-community/pms",
    "author": "Commit [Sun], Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "category": "Pms",
    "depends": ["base"],
    "auto_install": False,
    "installable": True,
}
