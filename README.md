# Flectra Community / pms

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[pms_rooming_xls](pms_rooming_xls/) | 2.0.1.0.0| Rooming xlsx Management
[pms_housekeeping](pms_housekeeping/) | 2.0.1.0.1| Housekeeping
[pms](pms/) | 2.0.2.33.0| A property management system
[pms_l10n_es](pms_l10n_es/) | 2.0.2.6.3| PMS Spanish Adaptation
[payment_acquirer_multi_pms_properties](payment_acquirer_multi_pms_properties/) | 2.0.1.0.2| Payment Acquirer Multiproperty
[multi_pms_properties](multi_pms_properties/) | 2.0.1.0.0| Multi Properties Manager


